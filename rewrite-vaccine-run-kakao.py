# 사용자가 수정해야 할 부분
class user_input:
    bottom_x = 
    bottom_y = 
    top_x = 
    top_y = 
    search_time = 0.1 # - use when finding
    onlyLeft = False # True, False - use when finding
    listing = "count" # latitude, count - use when finding
    from_code = "List" # Map, List - use when reservation
    vac_code = "VEN00013" # No ANY on this code - use when reservation
    print_status = False


# ======================== code ======================== #

import json
import re
import time
from datetime import datetime
from playsound import playsound

import browser_cookie3

import requests
import urllib3
import sys
urllib3.disable_warnings()

# Close Function
def close(success=False):
    if success:
        playsound("sound/tada.mp3")
    else:
        playsound("sound/xylophon.mp3")
    input("Press Enter to Close...")
    sys.exit()


try:
    jar = browser_cookie3.chrome(domain_name=".kakao.com")
except Exception as e:
    print(e)
    close()



class headers:
    vac_map = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=utf-8",
        "Origin": "https://vaccine-map.kakao.com",
        "Accept-Language": "en-us",
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 14_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 KAKAOTALK 9.4.2",
        "Referer": "https://vaccine-map.kakao.com/",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "Keep-Alive",
        "Keep-Alive": "timeout=5, max=1000"
    }
    vac = {
        "Accept": "application/json, text/plain, */*",
        "Content-Type": "application/json;charset=utf-8",
        "Origin": "https://vaccine.kakao.com",
        "Accept-Language": "en-us",
        "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 14_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 KAKAOTALK 9.4.2",
        "Referer": "https://vaccine.kakao.com/",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "Keep-Alive",
        "Keep-Alive": "timeout=5, max=1000"
    }


def check_user_info():
    user_info_api = 'https://vaccine.kakao.com/api/v1/user'
    user_info_response = requests.get(user_info_api, headers=headers.vac, cookies=jar, verify=False)
    user_info_json = json.loads(user_info_response.text)
    if user_info_json.get('error'):
        print("사용자 정보를 불러오는데 실패하였습니다.")
        print("Chrome 브라우저에서 카카오에 제대로 로그인되어있는지 확인해주세요.")
        print("로그인이 되어 있는데도 안된다면, 카카오톡에 들어가서 잔여백신 알림 신청을 한번 해보세요. 정보제공 동의가 나온다면 동의 후 다시 시도해주세요.")
        close()
    else:
        user_info = user_info_json.get("user")
        if user_info['status'] == "NORMAL":
            print(f"{user_info['name']}님 안녕하세요.")
            input("Press Enter to Start...")
        elif user_info['status'] == "UNKNOWN":
            print("상태를 알 수 없는 사용자입니다. 1339 또는 보건소에 문의해주세요.")
            close()
        elif user_info['status'] == "REFUSED":
            print(f"{user_info['name']}님은 백신을 예약하고 방문하지 않은 사용자로 파악됩니다. 잔여백신 예약이 불가합니다.")
            close()
        elif user_info['status'] == "ALREADY_RESERVED" or user_info['status'] == "ALREADY_VACCINATED":
            print(f"{user_info['name']}님은 이미 예약 또는 접종이 완료된 사용자입니다.")
            close()
        else:
            print(f"알려지지 않은 상태 코드입니다. 상태코드:{user_info['status']}")
            print("상태 코드 정보와 함께 Issues 생성 부탁드립니다.")
            close()

def try_reservation(orgCode):
    reservation_url = 'https://vaccine.kakao.com/api/v2/reservation'
    reservation_data = {"from": user_input.from_code, "vaccineCode": user_input.vac_code, "orgCode": orgCode, "distance": None}
    reservation_response = requests.post(reservation_url, data=json.dumps(reservation_data), headers=headers.vac, cookies=jar, verify=False)

    reservation_response_json = json.loads(reservation_response.text)
    if reservation_response_json.get('error'):
        print("사용자 정보를 불러오는데 실패하였습니다.")
        print("Chrome 브라우저에서 카카오에 제대로 로그인되어있는지 확인해주세요.")
        close()
    else:
        if reservation_response_json['code'] == "NO_VACANCY":
            print("잔여백신 접종 신청이 선착순 마감되었습니다.")
        elif reservation_response_json['code'] == "TIMEOUT":
            print("TIMEOUT.")
        elif reservation_response_json['code'] == "SUCCESS":
            print("백신접종신청 성공!!! 카카오톡 지갑을 확인하세요.")
            close(success=True)
        else:
            print("ERROR. 아래 메시지를 보고, 예약이 신청된 병원 또는 1339에 예약이 되었는지 확인해보세요.")
            print(reservation_response.text)
            close()


def find_vaccine():
    map_api_url = 'https://vaccine-map.kakao.com/api/v3/vaccine/left_count_by_coords'
    map_api_data = {"bottomRight": {"x": user_input.bottom_x, "y": user_input.bottom_y}, "onlyLeft": user_input.onlyLeft, "order": user_input.listing, "topLeft": {"x": user_input.top_x, "y": user_input.top_y}}
    done = False
    found = None

    while True:
        try:
            time.sleep(user_input.search_time)
            print(f"Start Finding: {datetime.now()}")
            map_response = requests.post(map_api_url, data=json.dumps(map_api_data), headers=headers.vac_map, verify=False, timeout=5)
        
            map_response_json_data = json.loads(map_response.text)
            for x in map_response_json_data.get("organizations"):
                # 잔여백신이 존재하는 경우
                if x.get('status') == "AVAILABLE" or x.get('leftCounts') != 0:
                    try_reservation(x.get('orgCode'))
            # 잔여백신 예약 실패시 다음 병원 시도
            
            if user_input.print_status:
                for org in map_response_json_data["organizations"]:
                    if org.get('status') == "INPUT_YET":
                        print(f"잔여갯수: {org.get('leftCounts')}\t상태: {org.get('status')}\t기관명: {org.get('orgName')}\t주소: {org.get('address')}")

        except json.decoder.JSONDecodeError as decodeerror:
            print("JSONDecodeError : ", decodeerror)
            print("JSON string : ", map_response.text)
            close()
            
        except requests.exceptions.Timeout as timeouterror:
                print("Timeout Error : ", timeouterror)

        except requests.exceptions.SSLError as sslerror:
            print("SSL Error : ", sslerror)
            close()

        except requests.exceptions.ConnectionError as connectionerror:
            print("Connection Error : ", connectionerror)
            # See psf/requests#5430 to know why this is necessary.
            if not re.search('Read timed out', str(connectionerror), re.IGNORECASE):
                close()

        except requests.exceptions.HTTPError as httperror:
            print("Http Error : ", httperror)
            close()

        except requests.exceptions.RequestException as error:
            print("AnyException : ", error)
            close()


def main_function():
    check_user_info()
    find_vaccine()

# ===================================== run ===================================== #
if __name__ == '__main__':
    main_function()
